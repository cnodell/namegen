#!/usr/bin/python

#Import Tkinter
try:
    from Tkinter import *
except:
    from tkinter import *
import random


#Create the application class
class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.level_state = "full"
        self.grid()
        self.createWidgets()
        self.first_letters = self.load_file('fl')
        self.v_following = self.load_file('v')
        self.c_following = self.load_file('c')
        self.v = ['a', 'e', 'i', 'o', 'u', 'y']
        self.lengths = [2,3,3,4,4,4,5,5,5,6,6,7]
    
    def load_file(self, file_name):
        f = open(file_name, 'r')
        parts = f.read().split('\n')
        while '' in parts:
            parts.remove('')
        f.close
        return parts

    def createWidgets(self):        
        self.text_ent = Text(self, wrap=WORD)
        self.text_ent.grid(row = 1, column = 2)
        self.text_ent.focus_set()

        self.save_button = Button(self, text="Generate", command=self.click_generate)
        self.save_button.grid(row = 1, column = 1)

    def click_generate(self):
        self.text_ent.delete(1.0, END)
        text = ''
        for i in range(20):
            name = random.choice(self.first_letters)
            length = random.choice(self.lengths) - 1
            if name[0] in self.v:
                char_type = 'v'
            else:
                char_type = 'c'
            for x in range(length):
                if char_type == 'v':
                    name = name + random.choice(self.c_following)
                    char_type = 'c'
                else:
                    name = name + random.choice(self.v_following)
                    char_type = 'v'
            if text == '':
                text = name.capitalize()
            else:
                text = text + '\n' + name.capitalize()
        self.text_ent.insert(END, text)


#Start Applicatipn

#Create app object from Application class
app = Application()

#Define app attributes
app.master.title("GNames")
app.master.geometry("200x400")

#start app event loop
app.mainloop()
