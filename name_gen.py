#!/usr/bin/python
import random

def load_file(file_name):
    f = open(file_name, 'r')
    parts = f.read().split('\n')
    while '' in parts:
	parts.remove('')
    f.close
    return parts


first_letters = load_file('fl')
v_following = load_file('v')
c_following = load_file('c')
v = ['a', 'e', 'i', 'o', 'u', 'y']
lengths = [2,3,3,4,4,4,5,5,5,6,6,7]

for i in range(500):
    name = random.choice(first_letters)
    #print 'Start with', name
    length = random.choice(lengths) - 1
    #print 'add', str(length), 'parts'
    if name[0] in v:
	char_type = 'v'
	#print 'first part is a v'
    else:
	char_type = 'c'
	#print 'first part is a c'
    for x in range(length):
	if char_type == 'v':
	    name = name + random.choice(c_following)
	    char_type = 'c'
	else:
	    name = name + random.choice(v_following)
	    char_type = 'v'
    print name.capitalize()
