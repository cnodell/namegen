#!/usr/bin/python
import random

def save_file(lst, name):
    text = ''
    for item in lst:
	if text == '':
	    text = item
	else:
	    text = text + '\n' + item
    f = open(name, 'w')
    f.write(text)
    f.close()

f = open('names', 'r')
names = f.read().split('\n')
names.remove('')
f.close
v = ['a', 'e', 'i', 'o', 'u', 'y']
c = ['q', 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x',
     'c', 'v', 'b', 'n', 'm']
fl = []
cg = []
vg = []
lengths = []

for name in names:
    name = list(name)
    if name[0] in v:
	part = ''
	try:
	    while name[0] in v:
		part = part + name.pop(0)
	    fl.append(part)
	except:
	    fl.append(part)
    elif name[0] in c:
	part = ''
	try:
	    while name[0] in c:
		part = part + name.pop(0)
	    fl.append(part)
	except:
	    fl.append(part)
    else:
	print name


for name in names:
    name = list(name)
    if name[0] in v:
	part = ''
	try:
	    while name[0] in v:
		part = part + name.pop(0)
	except:
	    pass
    elif name[0] in c:
	part = ''
	try:
	    while name[0] in c:
		part = part + name.pop(0)
	except:
	    pass
    else:
	print name
    while name != []:
        if name[0] in v:
            part = ''
            try:
                while name[0] in v:
                    part = part + name.pop(0)
                vg.append(part)
            except:
                vg.append(part)
        elif name[0] in c:
            part = ''
            try:
                while name[0] in c:
                    part = part + name.pop(0)
                cg.append(part)
            except:
                cg.append(part)
	else:
	    print name

save_file(fl, 'fl')
save_file(cg, 'c')
save_file(vg, 'v')
